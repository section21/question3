const http = require('https');

const args = process.argv.slice(2);
const options = {
	method: 'GET',
	hostname: 'codequiz.azurewebsites.net',
	port: null,
	path: '/',
	headers: {
		'Content-Length': '0',
		Cookie: ['hasCookie=true'],
	},
};

const req = http.request(options, function (res) {
	const chunks = [];

	res.on('data', function (chunk) {
		chunks.push(chunk);
	});

	res.on('end', function () {
		let data = {};
		const body = Buffer.concat(chunks);

		const table = body.toString().split('<tr>').slice(2);

		table.map((tag, i) => {
			const row = tag
				.replace('</tr>', '')
				.replace(/<td>/g, '')
				.trim()
				.split('</td>');
			data = {
				...data,
				[row[0].trim()]: row[1].trim(),
			};
		});
		args.forEach((arg) => {
			console.log(arg, 'is', data[arg]);
		});
	});
});

req.end();
